import argparse, sys, os
from zipfile import ZipFile

SOURCE = ""
SCRIPTS = "scripts"
ENTRY = "run.sh"

parser = argparse.ArgumentParser()
parser.add_argument("--source", type=str, default=SOURCE)
parser.add_argument("--scripts", type=str, default=SCRIPTS)
parser.add_argument("--entry", type=str, default=ENTRY)
args = parser.parse_args()

source = args.source
scripts = os.path.abspath(args.scripts)
entry = args.entry

os.makedirs(scripts, exist_ok=True)

if source:
    fileName = os.path.basename(source)
    if not fileName.endswith(".zip"): sys.exit("Import file must be a zip")
    dest = os.path.join(scripts, fileName)

    if source.startswith("s3"):
        os.system("aws s3 cp \"{}\" \"{}\"".format(source, dest))
    elif source.startswith("http"):
        os.system("curl -o \"{}\" \"{}\"".format(dest, source))
    ZipFile(dest, "r").extractall(scripts)
    
os.system("cd \"{}\" && bash \"{}\"".format(scripts, entry))