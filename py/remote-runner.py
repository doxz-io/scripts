import argparse, sys, os
from zipfile import ZipFile

SOURCE = ""
DEST = "scripts"
ENTRY = "entry.sh"

parser = argparse.ArgumentParser()
parser.add_argument("--source", type=str, default=SOURCE)
parser.add_argument("--dest", type=str, default=DEST)
parser.add_argument("--entry", type=str, default=ENTRY)
args = parser.parse_args()

source = args.source
if not source: sys.exit("Source must not be null")

fileName = os.path.basename(source)
if not fileName.endswith(".zip"): sys.exit("Import file must be a zip")

scripts = os.path.abspath(args.scripts)
entry = args.entry
dest = os.path.join(scripts, fileName)




os.makedirs(scripts, exist_ok=True)

if source.startswith("s3"):
    os.system("aws s3 cp \"{}\" \"{}\"".format(source, dest))
elif source.startswith("http"):
    os.system("curl -o \"{}\" \"{}\"".format(dest, source))
else:
    sys.exit("Invalid source")

ZipFile(dest, "r").extractall(dest)
    
os.system("cd \"{}\" && bash \"{}\"".format(scripts, entry))